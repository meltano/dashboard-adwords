from setuptools import setup, find_packages

setup(
    name='dashboard-adwords',
    version='0.3',
    description='Meltano reports and dashboards for Google Ads data',
    packages=find_packages(),
    package_data={'reports': ['*.m5o'], 'dashboards': ['*.m5o'], 'snapshots': ['**/*.m5o']},
    install_requires=[],
)
